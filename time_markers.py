
import time
import ctypes
from datetime import datetime
from datetime import timedelta
from datetime import date
from dateutil.relativedelta import relativedelta
import configparser
import os



def isLocked():
    import win32process
    import win32api
    import win32gui
    whnd = win32gui.GetForegroundWindow()
    # (_, pid) = win32process.GetWindowThreadProcessId(whnd)
    # handle = win32api.OpenProcess(win32con.PROCESS_ALL_ACCESS, False, pid)
    # filename = win32process.GetModuleFileNameEx(handle, 0)
    window_text = win32gui.GetWindowText(whnd)
    return ((win32gui.GetForegroundWindow() == 0) or (window_text == 'Windows Default Lock Screen'))



class Common:
    def __init__(self):
        pass



class CommonTimePeriod(Common):
    startMarker = None
    stopMarker  = None

    def __init__(self):
        Common.__init__(self)



class CommonWorkingTimePeriod(CommonTimePeriod):
    def __init__(self):
        CommonTimePeriod.__init__(self)
        self.startMarker = datetime.now()

    def update(self):
        raise NotImplementedError()



class WorkTimeLog(CommonWorkingTimePeriod):
    def __init__(self):
        import holidays
        import vacation

        CommonWorkingTimePeriod.__init__(self)
        self.holidays = dict((sorted(holidays.PL(years=datetime.now().year).items())))
        self.vacation = vacation.days_used

        self.active = False

    def load_plugin(self, filename, context):
        source = open(filename).read()
        code = compile(source, filename, 'exec')
        exec(code, context)
        return context

    def update_month_balance(self, moth_date, durations):
        from calendar import monthrange

        file_name = "{}{:02d}.ini".format(moth_date.year, moth_date.month)
        section_key = "{}-{:02d}".format(moth_date.year, moth_date.month)
        config = configparser.ConfigParser()
        config.read(file_name)

        if not config.has_section(section_key):
            config.add_section(section_key)

        wd, numofdays = monthrange(moth_date.year, moth_date.month)
        expected = 0
        for day in range(0, numofdays):
            dayDate = date(moth_date.year, moth_date.month, day+1)
            if dayDate not in self.holidays and dayDate and dayDate.weekday() != 5 and dayDate.weekday() != 6:
                expected += 8
           
        done = timedelta(0)
        for key in durations.keys():
            if(key.year == moth_date.year) and (key.month == moth_date.month):
                done += durations[key]  
        done = (done.days*24 + done.seconds/60/60)

        vacationInMonth = self.vacation[moth_date.month-1]
        done  = round(done + (len(vacationInMonth) * 8), 2)

        config[section_key]["expected"] = str(expected)
        config[section_key]["done"] = str(done)

        with open(file_name, 'w') as configfile:    
            config.write(configfile)

    def update_time_balance(self, durations):
        file_name = "wtb.ini"
        section_key = "work time balance"
        config = configparser.ConfigParser()
        config.read(file_name)

        if not config.has_section(section_key):
            config.add_section(section_key)

        expected = 8 * len(durations)
        done = timedelta(0)               
        for key in durations.keys():
            done += durations[key]  
        done = round((done.days*24 + done.seconds/60/60), 2)

        config[section_key]["expected"] = str(expected)
        config[section_key]["done"] = str(done)

        with open(file_name, 'w') as configfile:    
            config.write(configfile)

    #update duration for selected day
    def update_duration(self, working_day):
        config = configparser.ConfigParser()
        file_name = "{}{:02d}{:02d}.ini".format(working_day.year, working_day.month, working_day.day)
        dayKey = "{}-{:02d}-{:02d}".format(working_day.year, working_day.month, working_day.day)

        duration = timedelta(0)
        if os.path.isfile(file_name):      
            config.read(file_name)

            start = datetime.strptime(config[dayKey]['start'], "%H:%M:%S")
            end = datetime.strptime(config[dayKey]['end'], "%H:%M:%S")
            duration = end - start

            config[dayKey]['duration'] = str(duration)
            config[dayKey]['remaining'] = str(round((duration - timedelta(hours = 8)).total_seconds()/60/60, 2))

            with open(file_name, 'w') as configfile:    
                config.write(configfile)

        return working_day, duration

    #update old durations
    def update_durations(self):
        durations = {}
        config = configparser.ConfigParser()

        wt_date = datetime(2019, 1, 1)
        end_date = datetime.now() - timedelta(days=1) #yesterday
        while wt_date < end_date:
            wt_date, duration = self.update_duration(wt_date)
            if duration:
                # durations.append(duration)
                durations[wt_date] = duration
            wt_date += timedelta(days=1)
        self.update_time_balance(durations)

        wt_date = datetime(2019, 1, 1)
        while wt_date < end_date:
            self.update_month_balance(wt_date, durations)
            wt_date += relativedelta(months=+1)

    #update current work time
    def update(self, active):
        from plyer import notification    

        if active:
            self.dayKey = datetime.now().strftime("%Y-%m-%d")
            self.fileName = datetime.now().strftime("%Y%m%d.ini")

            self.config = configparser.ConfigParser()
            self.config.read(self.fileName)

            if not self.config.has_section(self.dayKey):
                self.config.add_section(self.dayKey)
                self.config[self.dayKey]['start'] = datetime.now().strftime("%H:%M:%S")

            self.config[self.dayKey]['end'] = datetime.now().strftime("%H:%M:%S")

            with open(self.fileName, 'w') as configfile:    
                self.config.write(configfile)

            wd, updated_duration = self.update_duration(datetime.now())
            print(updated_duration)
            # notification.notify(
            #     title='Here is the title',
            #     message='Here is the message',
            #     app_name='Here is the application name'#,                app_icon='path/to/the/icon.png'
            # )            


    def grabPic(self, active):
        if(active is True):
            if self.active is False:
                fileName = "{}{:02d}{:02d}_{:02d}{:02d}{:02d}.jpg".format(datetime.now().year, datetime.now().month, datetime.now().day, datetime.now().hour, datetime.now().minute, datetime.now().second)
                from ecapture import ecapture as ec
                ec.capture(0,False,fileName)
        self.active = active


def main():
    print("--------------")

    wtl = WorkTimeLog()

    context = {}
    func = wtl.load_plugin("./plugins/cmn.py", context)
    func["init"]()
    func["run"]()
    func["close"]()

    wtl.update_durations()
    
    while 1:
        if isLocked():
            print("locked")

        wtl.grabPic(not isLocked())
        wtl.update(not isLocked())

        time.sleep (2)



if __name__== "__main__":
    main()

