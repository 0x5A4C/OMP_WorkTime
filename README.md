# OMP_WorkTime

Very simple (really) work time logger.

Features:
 - detect locking/unlocking workstation
 - first unlock during day is working day start
 - last lock during day i working day end
 - calculate work time balance for day (YYYYMMDD.ini) 
 - calculate work time balance (wtb.ini): counts logged days expected working time, counts worked time
 - calculate work time for calendar moths covering logged working days(YYYYMM.ini): counts working days (official holidays are skipped) expected working time, counts worked time
 - take into account holidays

ToDo:
 - trigger updating time balances calculation